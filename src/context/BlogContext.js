import createDataContext from './createDataContext';
import { useCallback } from 'react';

const blogReducer = (state, action) => {
    switch (action.type){

        case 'edit_blogpost':
            return state.map((blogPost) =>{
            if(blogPost.id === action.payload.id){
                return action.payload;
            } else {
                return blogPost;
            }
        })
        case 'add_blogpost' :
            return [...state, 
                { 
                    id: Math.floor(Math.random()*99999) , 
                    title: action.payload.title,
                    content: action.payload.content
                }];
        // filter returns boolean depending on result. not sure why
        case 'delete_blogpost':
            return state.filter((blogPost) => blogPost.id !== action.payload);
        default:
            return state;
    }
};

// 9. I think this is callback which runs when dispatch() called.
const addBlogPost = dispatch => {
    return (title, content, callback) => {
        dispatch( {type: 'add_blogpost', payload: {title, content}});
        callback();
    };
};

const deleteBlogPost = dispatch => {
    //not sure how but somehow id gets passed here
    return (id) => {
        dispatch( {type: 'delete_blogpost', payload: id});
    };
};

const editBlogPost = dispatch => {
    return (id, title, content, callback) => {
        dispatch({type: 'edit_blogpost', payload: {id, title, content}});
        callback();
    };
};

// 5. E.g. here, BlogContext exports the Context and Provider so it can be used here...
export const {Context, Provider} = createDataContext(blogReducer, {addBlogPost, deleteBlogPost, editBlogPost}, []);