import React, {useState} from 'react';
import { View, Text, StyleSheet, TextInput, Button  } from 'react-native';

const BlogPostForm = ({onSubmit, initialValues }) => {

    const [title, setTitle] = useState(initialValues.title);
    const [content, setContent] = useState(initialValues.content);


    return (
        <View>
        <TextInput placeholder={'Enter Title'} onChangeText={(text) => setTitle(text)} style={style.textInput} />
        <TextInput placeholder='Enter Content'onChangeText={(text)=> setContent(text)} style={style.textInput} />
        <Button title={'Save Blog'} 
        onPress={ () => onSubmit(title, content) }
  
            />
    </View>

    );

};


BlogPostForm.defaultProps = {
    initialValues: {
        title: '',
        content: ''
    }

};

const style = StyleSheet.create({
    textInput: {
        fontSize: 20
    }
});

export default BlogPostForm;