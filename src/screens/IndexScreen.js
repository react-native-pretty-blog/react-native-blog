import React, {useContext} from 'react';
import { View, Text, StyleSheet, FlatList, Button, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

// 6. Context used here. 

import { Context } from '../context/BlogContext';


const IndexScreen = ({navigation}) => {

    // 7. Specifically here, we can extract the
    // state and bound actions which we initally set up here ...
    const {state, deleteBlogPost} = useContext(Context);

    return (
        <View>
            <FlatList 
            data={state}
            keyExtractor = {(state) => state.title }
            renderItem={ ({item}) => {
                return (
                <TouchableOpacity onPress = {() => navigation.navigate('Show', {id: item.id})}>
                    <View style={styles.row}>
                    <Text style={styles.cardTitle}> {item.title} - {item.id} </Text>
                    <TouchableOpacity onPress = { ()=> deleteBlogPost(item.id)}>
                    <Icon name='delete' style={styles.icon} />
                    </TouchableOpacity>
                    </View>
                </TouchableOpacity>
                );
            }}
            />
        </View>

    );

};


const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderColor: 'gray'
    },
    title: {
        fontSize: 20,
        textAlign: 'center'
    },
    cardTitle: {
        fontSize: 18
    },
    icon: {
        fontSize: 24,
        color: 'gray'
    }
});

IndexScreen.navigationOptions = ({navigation}) => {
    return {
        headerRight: 
    
            <TouchableOpacity onPress={ () =>navigation.navigate('Create') }>
            <Icon name='add' style={styles.icon} />
        </TouchableOpacity>
        
    };
};
export default IndexScreen;