import React, {useState, useContext} from 'react';
import { View, Text, StyleSheet, TextInput, Button } from 'react-native';
import {Context } from '../context/BlogContext';
import BlogPostForm from '../components/BlogPostFrom';

const EditScreen = ({navigation}) => {

    const {state, editBlogPost} = useContext(Context);
    const id = navigation.getParam('id');
    const blogPost = state.find((blogPost) => blogPost.id === id )
   
    return (
     <BlogPostForm
     initialValues = {{ title: blogPost.title, content: blogPost.content}}

     onSubmit={
         (title, content) => {
             editBlogPost(id, title, content, ()=> navigation.pop());
         }
     }
     
     />

    );

};


const style = StyleSheet.create({
    TextInput: {
        fontSize: 20,
  
    }
});

export default EditScreen;