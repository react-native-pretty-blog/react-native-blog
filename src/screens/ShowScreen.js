import React, {useState, useContext} from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Context } from '../context/BlogContext';
import Icon from 'react-native-vector-icons/MaterialIcons';

const ShowScreen = ({navigation}) => {
    const id = (navigation.getParam('id'));

    const { state } = useContext(Context);
    const blogPost = state.find((blogPost) => blogPost.id === navigation.getParam('id'));

    return (
        <View>
            <Text style={style.title}> {blogPost.title} </Text>
            <Text style={style.content}> {blogPost.content} </Text>

        </View>

    );

};

ShowScreen.navigationOptions = ({navigation}) => {
    return {
        headerRight: 
    
            <TouchableOpacity onPress={ () =>navigation.navigate('Edit', {id: navigation.getParam('id')}) }>
            <Icon name='edit' style={style.icon} />
        </TouchableOpacity>
        
    };
};

const style = StyleSheet.create({
    title: {
        fontSize: 20
    },
    icon: {
        fontSize: 20
    }
});

export default ShowScreen;