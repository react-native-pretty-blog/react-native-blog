import React, { useContext} from 'react';
import { View, Text, StyleSheet, TextInput, Button } from 'react-native';
import {Context} from '../context/BlogContext';
import BlogPostFrom from '../components/BlogPostFrom';

const CreateScreen = ({navigation}) => {
   
    const {addBlogPost} = useContext(Context);

    return (
        <BlogPostFrom

        onSubmit={(title, content) => { 
            addBlogPost(title, content, ()=> navigation.navigate('Index'));
        }}
        
        />
       
    );

};


const style = StyleSheet.create({
    
});

CreateScreen.navigationOptions = (navigation) => {
    return {
        title: 'Create a Blog'
    }
};

export default CreateScreen;